﻿using Dungeonator;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace ETG_SeedSync_Gunnect
{
    // All non-Gunnect+ code was copied from Apache Thunder's Gungeon Seed Mod
    // https://github.com/ApacheThunder/GungeonSeedMod/blob/master/GungeonSeedMod/GungeonSeedMod.cs
    public class ETG_SeedSync_Gunnect : ETGModule
    {
        public const string GUNNECT_CLIENT_ID = "ETG_Gunnect_Client";
        public const string MOD_ID = "ETG_SeedSync_Gunnect";

        private static ETGModule GunnectClient;
        private static MethodInfo GunnectInvokeMethod;

        public override void Init() { }

        public override void Start()
        {
            this.SetupGunnect();

            ETGModConsole.Commands.AddGroup("seedsync", delegate (string[] e) {
                ETGModConsole.Log("[Gungeon Seed]  The following options are available for setting Gungeon Seeds:");
                ETGModConsole.Log("checkseed\nsetseed\nnewseed\nreset\n\nTo set a custom seed, use 'setseed SEED1 SEED2' where SEED1 is the main Gungeon seed between 1 and 1000000000 and\n SEED2 is the RandomIntForRun seed. Set Seed2 to a value of 0 to 1000.\nNote that setting SEED1 to zero will disable seeded runs.\nSEED2 is not required. If not specified, a default value of 256 will be used.\nTo ensure consistant results, set your seed after selecting a character at the breach but before leaving the breach.\nSetting a seed while in the middle of a run or via quick restart is not recommended.\nTo start a new seed, simply use 'seedsync newseed' and write down the displayed seed if you want to share it!.\nUse 'seedsync checkseed' to check your current seed.\n\nTo turn off custom seed mode, use 'seedsync reset' or set SEED1 argument of setseed to zero.");
            });
            ETGModConsole.Commands.GetGroup("seedsync").AddUnit("checkseed", delegate (string[] e) { InitializeSeed(CheckSeed: true); });
            ETGModConsole.Commands.GetGroup("seedsync").AddUnit("setseed", SetSeed);
            ETGModConsole.Commands.GetGroup("seedsync").AddUnit("newseed", delegate (string[] e) {
                InitializeSeed(BraveRandom.GenerationRandomRange(0, 1000000000), Random.Range(0, 1000), false);
            });
            ETGModConsole.Commands.GetGroup("seedsync").AddUnit("reset", delegate (string[] e) {
                InitializeSeed(RandomIntForCurrentRun: Random.Range(0, 1000));
                ETGModConsole.Log("Custom seeds has been disabled. Please start a new run to ensure changes take effect.");
            });

        }

        private void SetupGunnect()
        {
            GunnectClient = ETGMod.AllMods.SingleOrDefault(x => x.GetType().Name == GUNNECT_CLIENT_ID);
            if (GunnectClient != null)
            {
                GunnectInvokeMethod = GunnectClient.GetType().GetMethod("ExternalModInvoke");
            }
        }

        public static void GunnectMessageHandler(int fromClientId, string fromClientUsername, string dataTypeName, string data)
        {
            switch (dataTypeName)
            {
                case "SetSeedMessage":
                    var setSeedData = JsonConvert.DeserializeObject<SetSeedMessage>(data);

                    ETGModConsole.Log("");
                    ETGModConsole.Log($"{fromClientUsername} has updated the seed!");
                    ETGModConsole.Log("");

                    InitializeSeed(setSeedData.DungeonSeed, setSeedData.RandomIntForCurrentRun, false);
                    break;
            }
        }

        private static bool SeedArgCount(string[] args, int min, int max)
        {
            if (args.Length >= min && args.Length <= max) return true;
            if (min == max)
            {
                ETGModConsole.Log("Error: need exactly " + min + " argument(s)");
            }
            else
            {
                ETGModConsole.Log("Error: need between " + min + " and " + max + " argument(s)");
            }
            return false;
        }

        private void SetSeed(string[] args)
        {
            Dungeon dungeon = GameManager.Instance.Dungeon;

            if (!SeedArgCount(args, 1, 2)) { return; }

            if (int.Parse(args[0]) > 1000000000)
            {
                ETGModConsole.Log("Error: Requested SEED1 value exceeds normal maximum value of 1000000000");
                return;
            }

            // Use default if second seed not specified.
            if (args.Length == 2)
            {

                if (int.Parse(args[0]) == 0) { ETGModConsole.Log("Warning: Setting a seed of 0 will disable seeded runs!"); }

                if (int.Parse(args[1]) > 1000)
                {
                    ETGModConsole.Log("Error: Requested RandomIntForRun seed exceeds normal maximum value of 1000");
                    return;
                }

                if (dungeon.LevelOverrideType != GameManager.LevelOverrideState.FOYER)
                {
                    ETGModConsole.Log("Warning: Setting a seed while not at the Breach can result in inconsistant results!");
                }

                InitializeSeed(int.Parse(args[0]), int.Parse(args[1]), false);
            }
            else
            {
                ETGModConsole.Log("Notice: SEED2 not specified. Using 256 as default value.");

                if (int.Parse(args[0]) == 0) { ETGModConsole.Log("Warning: Setting a seed of 0 will disable seeded runs!"); }

                if (dungeon.LevelOverrideType != GameManager.LevelOverrideState.FOYER)
                {
                    ETGModConsole.Log("Warning: Setting a seed while not at the Breach can result in inconsistant results!");
                }
                // If SEED2 not specified, a default value of 256 will be used.
                InitializeSeed(int.Parse(args[0]), 256, false);

                if (GunnectClient != null)
                {
                    var message = new SetSeedMessage
                    {
                        DungeonSeed = int.Parse(args[0]),
                        RandomIntForCurrentRun = int.Parse(args[1])
                    };
                    var serializedMessage = JsonConvert.SerializeObject(message);

                    GunnectInvokeMethod.Invoke(null, new object[] { 1, null, MOD_ID, message.GetType().Name, serializedMessage });
                }
            }
            return;
        }

        private static void InitializeSeed(int DungeonSeed = 0, int RandomIntForCurrentRun = 0, bool CheckSeed = false)
        {
            if (CheckSeed)
            {
                ETGModConsole.Log("");
                ETGModConsole.Log("IsSeeded Status: " + GameManager.Instance.IsSeeded.ToString());
                ETGModConsole.Log("CurrentRunSeed: " + GameManager.Instance.CurrentRunSeed);
                ETGModConsole.Log("RandomIntForRun: " + GameManager.Instance.RandomIntForCurrentRun);
                ETGModConsole.Log("GetDungeonSeed Test: " + GameManager.Instance.Dungeon.GetDungeonSeed());
                ETGModConsole.Log("If 'GetDungeonSeed Test' returns a random value on multiple checks, seeded runs is not active.");
                ETGModConsole.Log("");
                return;
            }
            ETGModConsole.Log("");
            ETGModConsole.Log("Old CurrentRunSeed: " + GameManager.Instance.CurrentRunSeed);
            ETGModConsole.Log("Old RandomIntForCurrentRun: " + GameManager.Instance.RandomIntForCurrentRun);
            GameManager.Instance.Dungeon.DungeonSeed = DungeonSeed;
            GameManager.Instance.CurrentRunSeed = DungeonSeed;
            GameManager.Instance.RandomIntForCurrentRun = RandomIntForCurrentRun;
            GameManager.Instance.Dungeon.GetDungeonSeed();
            ETGModConsole.Log("New CurrentRunSeed: " + GameManager.Instance.CurrentRunSeed, false);
            ETGModConsole.Log("New RandomIntForCurrentRun: " + GameManager.Instance.RandomIntForCurrentRun);
            ETGModConsole.Log("");

            if (GunnectClient != null)
            {
                var message = new SetSeedMessage
                {
                    DungeonSeed = DungeonSeed,
                    RandomIntForCurrentRun = RandomIntForCurrentRun
                };
                var serializedMessage = JsonConvert.SerializeObject(message);

                GunnectInvokeMethod.Invoke(null, new object[] { 0, null, MOD_ID, message.GetType().Name, serializedMessage });
            }
        }

        public override void Exit() { }
    }
}
